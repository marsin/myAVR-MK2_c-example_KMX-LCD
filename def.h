/**********************************************************************
 myAVR MK2 example: SMX, LCD
   - example program for Switch Matrix (SMX) input and LCD output
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

/**********************************************************************
 * Components:
   - myAVR MK2 board
   - myAVR LCD V2.5 Add-On board
   - mySmartUSB MK2 programmer
 (www.myavr.com)


 * Dot Matrix LCD:
   - 2 lines, 16 columns, a 5x7 pixel

 * Switch Matrix
   - 5 lines, 5 rows

 * Controller:
   AVR ATmega8L, 3.6864 MHz

 * Function:

 * Circuit:
   PortB.0 := MX ROW0 - driver (blue)
   PortB.1 := MX ROW1
   PortB.2 := MX ROW2
   PortB.3 := MX ROW3
   PortB.4 := MX ROW4
   PortC.0 := MX COL0 - reader (white)
   PortC.1 := MX COL1
   PortC.2 := MX COL2
   PortC.3 := MX COL3
   PortC.4 := MX COL4

   PortB.0 := LCD R/W (read/write), JP set to Aus (off) - not used for LCD
   PortB.1 := LCD PWM (backlight),  JP set to An  (on)  - not used for LCD
   PortD.2 := LCD RS  (reset)
   PortD.3 := LCD E   (enable)
   PortD.4 := LCD DB4 (databit 4 of LCD)
   PortD.5 := LCD DB5 (databit 5 of LCD)
   PortD.6 := LCD DB6 (databit 6 of LCD)
   PortD.7 := LCD DB7 (databit 7 of LCD)
 **********************************************************************/

#ifndef MYAVR_MK2__SMX_LCD__DEF_H
#define MYAVR_MK2__SMX_LCD__DEF_H

#define F_CPU 3686400ul

#include <avr/io.h>
//#include <util/delay.h>    // TEST
#include <avr/interrupt.h>


// Define: LCD ports
#define LCD_PORT PORTD
#define LCD_DDR  DDRD
#define LCD_RS   PD2
#define LCD_E    PD3
#define LCD_D4   PD4
#define LCD_D5   PD5
#define LCD_D6   PD6
#define LCD_D7   PD7


// Define: Switch Matrix ports
#define SMX_DRIVER_PORT PORTB // (driver, rows, blue, -)
#define SMX_DRIVER_DDR  DDRB
#define SMX_D0 PB0
#define SMX_D1 PB1
#define SMX_D2 PB2
#define SMX_D3 PB3
#define SMX_D4 PB4

#define SMX_READER_PORT PORTC // (reader, cols, white, +)
#define SMX_READER_DDR  DDRC
#define SMX_READER_PIN  PINC
#define SMX_R0 PC0
#define SMX_R1 PC1
#define SMX_R2 PC2
#define SMX_R3 PC3
#define SMX_R4 PC4


#endif // MYAVR_MK2__SMX_LCD__DEF_H

