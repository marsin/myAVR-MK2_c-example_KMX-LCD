/**********************************************************************
 myAVR MK2 example: SMX, LCD
   - example program for Switch Matrix (SMX) input and LCD output
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#include "lcd.h"
#include "smx.h"


// Prototypes
void init (void);
void display_scancodes (void);
void display_char      (void);


/** Initialize Modules
 * init modules (lcd, smx)
 */
void init (void)
{
	lcd_init();
	init_smx();

	// Pins PB5 and PC5 used (connected) for test signal LEDs
	DDRB  |=  (1<<PB5); // set as output
	DDRC  |=  (1<<PC5); // set as output
	PORTB &= ~(1<<PB5); // init output LOW (LED off)
	PORTC &= ~(1<<PC5); // init output LOW (LED off)

	return;
}


/** Display Character
 * Look in all cells of FIFO buffer.
 * If buffer cell has a falling edge in,
 * print a character for this key on LCD.
 * Clear every buffer cell with any edge following.
 */
void display_char (void)
{
	static uint8_t pos_lcd  = 0;
	uint8_t        pos_fifo = 0;
	char           c[2]     = { '\0' };

	for (pos_fifo=0; pos_fifo < FIFO_SIZE; ++pos_fifo)
	{
		// check cell for an edge
//		if (EDGE_FALL == fifo_scancode[pos_fifo].edge
//			|| EDGE_RISE == fifo_scancode[pos_fifo].edge)
		if (EDGE_NONE != fifo_scancode[pos_fifo].edge)
		{
			// EDGE_FALL is the edge form not pressed to pressed switch
			if (EDGE_FALL == fifo_scancode[pos_fifo].edge)
			{
				// read fifo cell an interprete it as character
				c[0] = fifo_scancode[pos_fifo].row
					+ fifo_scancode[pos_fifo].col * 5 + 'a';

				// print character on LCD
				lcd_pos(pos_lcd/16, pos_lcd%16); // lcd_pos(<row>, <col>)
				lcd_text(&c[0]);

				// set next pos on LCD
				if (++pos_lcd >= 32)
				{
					pos_lcd = 0;
				}
			}
			// clear fifo cell
			fifo_scancode[pos_fifo].row  = 0;
			fifo_scancode[pos_fifo].col  = 0;
			fifo_scancode[pos_fifo].edge = EDGE_NONE;
		}
	}

	return;
}


/** Display Scan Codes
 * Look in all cells of FIFO buffer.
 * If buffer cell has a rising or a falling edge in,
 * print the key positions on LCD.
 * Clear cell following.
 */
void display_scancodes (void)
{
	uint8_t pos_fifo = 0;
	char
		str_row[2] = { '\0' },
		str_col[2] = { '\0' };

	for (pos_fifo=0; pos_fifo < FIFO_SIZE; ++pos_fifo)
	{
		// check cell
//		if (EDGE_FALL == fifo_scancode[pos_fifo].edge
//			|| EDGE_RISE == fifo_scancode[pos_fifo].edge)
		if (EDGE_NONE != fifo_scancode[pos_fifo].edge)
		{
			// display key code
			str_row[0] = fifo_scancode[pos_fifo].row + '0';
			str_col[0] = fifo_scancode[pos_fifo].col + '0';

			lcd_pos(0, 5);
			lcd_text(str_row);
			lcd_pos(1, 5);
			lcd_text(str_col);

			// clear fifo cell
			fifo_scancode[pos_fifo].row  = 0;
			fifo_scancode[pos_fifo].col  = 0;
			fifo_scancode[pos_fifo].edge = EDGE_NONE;
		}
	}

	return;
}


/** Main
 * when the flag 'poll' is set by timer ISR(),
 *   then disable the interrupts and call the poll switch matrix function.
 *   This function checks all switches for their state.
 *   Has a state chaged (from released to pressed or from pressed to released)
 *   then the EDGE becomes written to the FIFO and the flag 'fifo' set.
 * when the flag 'fifo' is set by the poll function,
 *   then disable the interrupts and check the fifo for edges,
 *   print a character for the switch.
 *   The FIFO becomes cleared in following.
 */
int main (void)
{
	init();

	// Text for dislpale_scancodes() function
//	lcd_pos(0, 0);
//	lcd_text("row: ");
//	lcd_pos(1, 0);
//	lcd_text("col: ");

	sei(); // enable interrupts

	while (1)
	{
		if (0 != flag_poll) // the poll flag is set by the timer ISR
		{
			cli();          // disable interrupts
			poll_smx();
			flag_poll = 0;
			sei();          // enable interrupts
		}

		if (0 != flag_fifo) // the fifo flag is set by the poll function
		{
			cli();
//			display_scancodes();
			display_char();
			flag_fifo = 0;
			sei();
		}
	}

	return 0;
}

